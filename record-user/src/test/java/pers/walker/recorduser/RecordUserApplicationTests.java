package pers.walker.recorduser;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pers.walker.recorduser.entity.User;
import pers.walker.recorduser.mapper.UserMapper;
import pers.walker.recorduser.service.UserService;

import java.util.List;

@SpringBootTest
class RecordUserApplicationTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @Test
    void contextLoads() {
        List<User> users = userMapper.selectList(null);

        System.out.println(users);
    }

}
