package pers.walker.recorduser.controller;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UserControllerTest {
    @Test
    void uuidTest() {
        System.out.println(UUID.fastUUID());
        System.out.println(UUID.randomUUID());
        System.out.println(IdUtil.fastUUID());
        System.out.println(IdUtil.randomUUID());
        System.out.println(IdUtil.simpleUUID());
        System.out.println(IdUtil.fastSimpleUUID());
        System.out.println(RandomUtil.randomInt(10^5));

    }

}