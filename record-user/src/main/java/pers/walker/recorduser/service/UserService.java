package pers.walker.recorduser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import pers.walker.recorduser.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sloera
 * @since 2021-03-30
 */
public interface UserService extends IService<User> {

    User getByUsername(String username);
}
