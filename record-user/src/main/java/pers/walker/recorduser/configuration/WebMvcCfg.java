package pers.walker.recorduser.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pers.walker.recorduser.interceptor.JwtInterceptor;

/**
 * JWT配置类，用于生成一个过滤器类，对"/record/*"下的所有访问资源进行JWT验证
 *
 * @author wxs
 * @Date 2021-03-27
 */
@Configuration
public class WebMvcCfg implements WebMvcConfigurer {


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JwtInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/favicon.ico","/v2/**", "/swagger-ui.html/**","/doc.html/**")
                .excludePathPatterns("/index/login", "/index/test")
                .excludePathPatterns("/user/login", "/user/register");
        ;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("*")
                .maxAge(30000);
    }
}
