package pers.walker.recorduser.configuration;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableKnife4j
public class SwaggerConfiguration {

    /**
     * 注入docket（swagger-ui）
     */
    @Bean
    protected Docket docket() {

        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Record")
                .description("Record接口文档")
                .version("1.0")
                .build();


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("pers.walker.recorduser.controller"))//扫描
                .paths(PathSelectors.any())
                .build();
    }
}
