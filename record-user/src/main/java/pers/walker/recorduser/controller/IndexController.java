package pers.walker.recorduser.controller;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import jwt.JwtTokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import pers.walker.recorduser.entity.User;

import java.util.HashMap;

@RestController
@Api(tags = "登陆页")
@RequestMapping("/index")
public class IndexController {

    private static final Logger log = LoggerFactory.getLogger(IndexController.class);

    @ApiOperation(value = "登录接口")
    @PostMapping("/login")
    public String login(@RequestBody User user) {
        if (StrUtil.isBlank(user.getUsername())) {
            return "用户名 username 不可为空";
        }
        log.info("用户登录成功");
        HashMap<String, String> map = new HashMap<>();
        map.put("username", user.getUsername());
        return JwtTokenUtil.getToken(map);
    }

    @ApiOperation(value = "测试接口")
    @GetMapping("/test")
    public String test() {

        log.info("用户访问成功");

        return "200";
    }
}
