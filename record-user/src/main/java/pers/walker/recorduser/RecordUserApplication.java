package pers.walker.recorduser;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
@MapperScan("pers.walker.recorduser.**.mapper")
public class RecordUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordUserApplication.class, args);
    }

}
