package pers.walker.recorduser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import pers.walker.recorduser.entity.User;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sloera
 * @since 2021-03-30
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
