package pers.walker.recordmoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecordMoneyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordMoneyApplication.class, args);
    }

}
