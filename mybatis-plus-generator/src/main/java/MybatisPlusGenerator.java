import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sloera
 * @version 1.0
 * @class PACKAGE_NAME.MybatisPlusGenerator
 * @date 2021-03-28
 */
public class MybatisPlusGenerator {
    private static final String PROJECT_PATH = "/Users/wang/Record/java/record/record-java/record-user";
    /**
     * 数据库地址
     */
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/record";
    /**
     * 数据库密码
     */
    private static final String PASSWORD = "123456";
    /**
     * 数据库用户名
     */
    private static final String USERNAME = "wang";
    /**
     * 数据库类型
     */
    private static DbType dbType = DbType.MYSQL;
    /**
     * 驱动类
     */
    private static final String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String SCHEMA_NAME = "mp";
    /**
     * 前缀，可将此数据库表的此前缀去除
     */
    private static final String TABLE_PREFIX = "";
    /**
     * 生成的表名
     */
    private static final String TABLE_NAME = "user";
    /**
     * 实体类等文件路径
     */
    private static final String PACKAGE_PATH = "src/main/java/pers/walker/recorduser";
    /**
     * 生成的实体类等 包名
     */
    private static final String PACKAGE_PARENT = "pers.walker.recorduser";

    public static void main(String[] args) {

        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        // 支持 AR 模式
        globalConfig.setActiveRecord(true)
                // 生成完成后，不打开文件管理器
                .setOpen(false)
                // 作者
                .setAuthor("sloera")
                // 生成路径
                .setOutputDir(PROJECT_PATH + "/src/main/java")
                // 文件覆盖
                .setFileOverride(true)
                // 主键策略
                .setIdType(IdType.AUTO)
                // 设置生成的 service 接口的名字首字母是否为I
                .setServiceName("%sService").setXmlName("%sMapper").setMapperName("%sMapper").setServiceImplName("%sServiceImpl").setControllerName("%sController").setBaseResultMap(true).setBaseColumnList(true).setSwagger2(false);
        // 数据源配置
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig.setDbType(dbType).setDriverName(DRIVER_NAME).setUrl(URL).setUsername(USERNAME).setPassword(PASSWORD).setSchemaName(SCHEMA_NAME);
        // 策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        // 全局大写名
        strategyConfig.setCapitalMode(true)
                // 数据库表映射到实体的命名策略
                .setNaming(NamingStrategy.underline_to_camel).setColumnNaming(NamingStrategy.underline_to_camel).setEntityLombokModel(true).setRestControllerStyle(true).setTablePrefix(TABLE_PREFIX)
                // 生成的表
                .setInclude(TABLE_NAME);
        // 包名策略配置
        PackageConfig packageConfig = new PackageConfig();
        Map<String, String> pathInfo = new HashMap<>();
        pathInfo.put("entity_path", PROJECT_PATH + "/" + PACKAGE_PATH + "/entity");
        pathInfo.put("mapper_path", PROJECT_PATH + "/" + PACKAGE_PATH + "/mapper");
        //        pathInfo.put("xml_path", "");
        pathInfo.put("service_path", PROJECT_PATH + "/" + PACKAGE_PATH + "/service");
        pathInfo.put("service_impl_path", PROJECT_PATH + "/" + PACKAGE_PATH + "/service/impl");
        // pathInfo.put("controller_path", PROJECT_PATH + "/" + PACKAGE_PATH + "/controller");
        packageConfig.setParent(PACKAGE_PARENT).setPathInfo(pathInfo);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return PROJECT_PATH + "/src/main/resources/mapper/" + packageConfig.getParent().replaceAll("\\.", "/") + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setController("");

        templateConfig.setXml(null);

        // 整合配置
        AutoGenerator autoGenerator = new AutoGenerator();
        autoGenerator.setGlobalConfig(globalConfig).setDataSource(dataSourceConfig).setStrategy(strategyConfig).setPackageInfo(packageConfig);
        autoGenerator.setCfg(cfg);
        autoGenerator.setTemplateEngine(new FreemarkerTemplateEngine());
        autoGenerator.setTemplate(templateConfig);

        autoGenerator.execute();
    }
}
