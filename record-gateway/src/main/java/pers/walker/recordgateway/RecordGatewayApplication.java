package pers.walker.recordgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecordGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordGatewayApplication.class, args);
    }

}
