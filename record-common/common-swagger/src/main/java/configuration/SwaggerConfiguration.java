package configuration;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

//@Configuration
//@EnableSwagger2
//@EnableKnife4j
public class SwaggerConfiguration {

    /**
     * 注入docket（swagger-ui）
     */
    @Bean
    protected Docket docket() {

        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Record")
                .description("Record接口文档")
                .version("1.0")
                .build();


        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("pers.walker"))//扫描
                .paths(PathSelectors.any())
                .build();
    }

}
