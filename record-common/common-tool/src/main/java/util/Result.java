package util;

import enums.CodeEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
/*
  通用返回结果实体

  @date 4/4/21
 */
public class Result<T> {

    private String code;
    private String message;
    private T data;


    public Result(CodeEnum codeEnum, T data) {
        this.code = codeEnum.getCode();
        this.message = codeEnum.getMessage();
        this.data = data;
    }

    public Result(CodeEnum codeEnum) {
        this(codeEnum, null);
    }


    public static <T> Result<T> success(T data) {
        return new Result<>(CodeEnum.SUCCESS, data);
    }

    public static <T> Result<T> success() {
        return new Result<>(CodeEnum.SUCCESS);
    }


    public static <T> Result<T> fail() {
        return new Result<>(CodeEnum.FAIL);
    }

    public static <T> Result<T> fail(CodeEnum code) {
        return new Result<>(code);
    }

    public static <T> Result<T> fail(T data) {
        return new Result<>(CodeEnum.FAIL, data);
    }

    public static <T> Result<T> fail(CodeEnum code, T data) {
        return new Result<>(code, data);
    }

}
