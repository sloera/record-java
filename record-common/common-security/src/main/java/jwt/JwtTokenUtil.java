package jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Map;

public class JwtTokenUtil {

    private static final String sign = "!#$%&*^22290";

    /**
     * 生成token
     *
     * @param map
     * @return
     */
    public static String getToken(Map<String, String> map) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, 2);//设置两小时token过期

        JWTCreator.Builder builder = JWT.create();
        //payload
        map.forEach(builder::withClaim);
        return builder.withExpiresAt(calendar.getTime())
                .sign(Algorithm.HMAC256(sign));
    }

    /**
     * 验证token的合法性
     *
     *
     * @param token
     * @return 返回token信息
     */
    public static DecodedJWT verify(String token) {
        return JWT.require(Algorithm.HMAC256(sign)).build().verify(token);
    }



}