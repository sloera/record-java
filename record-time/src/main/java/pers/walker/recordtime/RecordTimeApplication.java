package pers.walker.recordtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecordTimeApplication {

    public static void main(String[] args) {
        SpringApplication.run(RecordTimeApplication.class, args);
    }

}
